/* dtkLogger.cpp --- 
 * 
 * Author: Julien Wintz
 * Copyright (C) 2008-2011 - Julien Wintz, Inria.
 * Created: Thu Mar  1 17:19:52 2012 (+0100)
 * Version: $Id$
 * Last-Updated: mar. avril 24 15:04:50 2012 (+0200)
 *           By: Nicolas Niclausse
 *     Update #: 101
 */

/* Commentary: 
 * 
 */

/* Change log:
 * 
 */

#include "dtkLogger.h"
#include "dtkLogger_p.h"
#include "dtkLogDestination.h"

dtkLogger& dtkLogger::instance(void)
{
    static dtkLogger log;

    return log;
}

dtkLog::Level dtkLogger::level(void) const
{
    return d->level;
}

void dtkLogger::setLevel(dtkLog::Level level)
{
    d->level = level;
}

void dtkLogger::setLevel(QString level)
{

    if (level == "trace")
        d->level = dtkLog::Trace;
    else if (level == "debug")
        d->level = dtkLog::Debug;
    else if (level == "info")
        d->level = dtkLog::Info;
    else if (level == "warn")
        d->level = dtkLog::Warn;
    else if (level == "error")
        d->level = dtkLog::Error;
    else if (level == "fatal")
        d->level = dtkLog::Fatal;
}

void dtkLogger::attachConsole(void)
{
    d->destinations << d->console;
}

void dtkLogger::detachConsole(void)
{
    d->destinations.removeOne(d->console);
}

void dtkLogger::attachFile(const QString& path)
{
    if(d->files.contains(path))
        return;

    d->files[path] = dtkLogDestinationPointer(new dtkLogDestinationFile(path));

    d->destinations << d->files[path];
}

void dtkLogger::detachFile(const QString& path)
{
    if(!d->files.contains(path))
        return;

    d->destinations.removeOne(d->files[path]);

    d->files.remove(path);
}

void dtkLogger::attachText(QPlainTextEdit *editor)
{
    if(d->editors.contains(editor))
        return;

    d->editors[editor] = dtkLogDestinationPointer(new dtkLogDestinationText(editor));

    d->destinations << d->editors[editor];
}

void dtkLogger::detachText(QPlainTextEdit *editor)
{
    if(!d->editors.contains(editor))
        return;

    d->destinations.removeOne(d->editors[editor]);

    d->editors.remove(editor);
}

void dtkLogger::attachModel(dtkLogModel *model)
{
    if(d->models.contains(model))
        return;

    d->models[model] = dtkLogDestinationPointer(new dtkLogDestinationModel(model));

    d->destinations << d->models[model];
}

void dtkLogger::detachModel(dtkLogModel *model)
{
    if(!d->models.contains(model))
        return;

    d->destinations.removeOne(d->models[model]);

    d->models.remove(model);
}

dtkLogger::dtkLogger(void) : d(new dtkLoggerPrivate)
{
    d->level = dtkLog::Info;

    d->console = dtkLogDestinationPointer(new dtkLogDestinationConsole);
}

dtkLogger::~dtkLogger(void)
{
    delete d;

    d = NULL;
}

void dtkLogger::write(const QString& message)
{
    for(int i = 0; i < d->destinations.count(); i++)
        d->destinations.at(i)->write(message);
}
